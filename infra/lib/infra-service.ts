import * as core from "@aws-cdk/core";
import { Bucket, BlockPublicAccess } from '@aws-cdk/aws-s3';
import { BucketDeployment, Source, CacheControl } from '@aws-cdk/aws-s3-deployment';
import { OriginAccessIdentity, CloudFrontWebDistribution } from '@aws-cdk/aws-cloudfront';

export class GithubSearchSampleInfraService extends core.Construct {
  constructor(scope: core.Construct, id: string) {
    super(scope, id);

    // Prepare Web App Deployment bucket
    const webAppBucket = new Bucket(this, 'sample-bdd-webapp', {
      blockPublicAccess: BlockPublicAccess.BLOCK_ALL,
      removalPolicy: core.RemovalPolicy.DESTROY,
      autoDeleteObjects: true,
      websiteIndexDocument: 'index.html',
    });

    new core.CfnOutput(this, '_WebApp-Bucket_', { value: webAppBucket.bucketName });

    // Prepare Web App Cloudfront OAI
    const webAppOAI = new OriginAccessIdentity(this, 'webapp-oai', { 
      comment: 'WebApp OAI'
    });

    // Allow read to Web App Cloudfront OAI
    webAppBucket.grantRead(webAppOAI);

    // Prepare Web App Cloudfront distro
    const webAppDistro = new CloudFrontWebDistribution(this, 'web-app-cf-distro', {
      originConfigs: [{
        behaviors: [{ isDefaultBehavior: true }],
        s3OriginSource: {
          s3BucketSource: webAppBucket,
          originAccessIdentity: webAppOAI,
        },
      }],
      errorConfigurations: [{
        errorCode: 404,
        responseCode: 200,
        responsePagePath: '/index.html'
      }],
    });

    new core.CfnOutput(this, '_WebApp-Distro-Id_', { value: webAppDistro.distributionId });
    new core.CfnOutput(this, '_WepApp-Distro-Domain-Name_', { value: webAppDistro.distributionDomainName });

    // Deploy Web App Build Contents
    new BucketDeployment(this, 'WebAppOtherFiles', {
      sources: [Source.asset('../webapp/build/', { exclude: ['index.html'] })],
      destinationBucket: webAppBucket,
      cacheControl: [CacheControl.fromString('max-age=31536000,public,immutable')],
      prune: false,
    });

    new BucketDeployment(this, 'WebAppIndexFile', {
      sources: [Source.asset('../webapp/build/', { exclude: ['*', '!index.html'] })],
      destinationBucket: webAppBucket,
      cacheControl: [CacheControl.fromString('max-age=0,no-cache,no-store,must-revalidate')],
      prune: false,
    });
  }
}