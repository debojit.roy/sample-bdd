describe("Github User Search", () => {
  it("A user lands on the home page", () => {
    cy.visit("http://localhost:3000");
  });

  it("the Browser Page Title should be - GitHub User Search", () => {
    cy.title().should("eq", "GitHub User Search");
  });

  it("page Top Heading should be - GitHub User Search", () => {
    cy.get("h1").contains("GitHub User Search");
  });

  it("when the user has not focused on the search input, must see the default placeholder - Type Username", () => {
    cy.get("input").should("have.attr", "placeholder", "Type Username");
  });

  it("when the user has focused on the search input, the user must be able to type a username for search", () => {
    cy.get("input").type("tom");
  });

  context("when the user enters search text and hits enter", () => {
    it("should not search if the search text entered by the user is empty", () => {
      cy.get("input").clear().type("{enter}");
      cy.get("Results Found").should("not.exist");
    });

    it("when search is in flight, should show - In Progress", () => {
      cy.intercept("GET", "**/users?**", {
        statusCode: 200,
        body: JSON.stringify({
          total_count: 100,
          incomplete_results: false,
          items: [],
        }),
        delay: 1000,
      }).as("getUser");

      cy.get("input").clear().type("debo").type("{enter}");
      cy.contains("In Progress");
      cy.wait("@getUser");
    });

    it("when count is greater than 0, should show results counts", () => {
      cy.intercept("GET", "**/users?**", {
        statusCode: 200,
        body: JSON.stringify({
          total_count: 100,
          incomplete_results: false,
          items: [],
        }),
      });

      cy.get("input").clear().type("debo").type("{enter}");
      cy.contains("100 Results Found");
    });

    it("when output is 0, should show 0 results counts", () => {
      cy.intercept("GET", "**/users?**", {
        statusCode: 200,
        body: JSON.stringify({
          total_count: 0,
          incomplete_results: false,
          items: [],
        }),
      });

      cy.get("input").clear().type("tilly").type("{enter}");
      cy.contains("0 Results Found");
    });

    it("when api throws error, should display error", () => {
      cy.intercept("GET", "**/users?**", {
        statusCode: 500,
        body: JSON.stringify({
          error: "Server Error",
        }),
      });

      cy.get("input").clear().type("tommy").type("{enter}");
      cy.contains("Failed to search User");
    });
  });

  context("when search results are available", () => {
    it("should display the user avatar and username", () => {
      cy.intercept("GET", "**/users?**", {
        statusCode: 200,
        body: JSON.stringify({
          total_count: 2,
          incomplete_results: false,
          items: [
            {
              id: 1,
              login: "debo",
              avatar_url: "https://avatars.githubusercontent.com/u/493257?v=4",
            },
            {
              id: 2,
              login: "DeborahK",
              avatar_url: "https://avatars.githubusercontent.com/u/7987365?v=4",
            },
          ],
        }),
      }).as("getUser");

      cy.get("input").clear().type("debo").type("{enter}");
      cy.wait("@getUser").then(() => {
        cy.get(
          'img[src="https://avatars.githubusercontent.com/u/493257?v=4"]'
        ).should("have.attr", "alt", "debo");
        cy.contains("debo");
        cy.get(
          'img[src="https://avatars.githubusercontent.com/u/7987365?v=4"]'
        ).should("have.attr", "alt", "DeborahK");
        cy.contains("DeborahK");
      });
    });

    it("should NOT display any user avatar when results are empty", () => {
      cy.intercept("GET", "**/users?**", {
        statusCode: 200,
        body: JSON.stringify({
          total_count: 0,
          incomplete_results: false,
          items: [],
        }),
      }).as("getUser");

      cy.get("input").clear().type("unknown").type("{enter}");
      cy.wait("@getUser").then(() => {
        cy.get(
          'img[src="https://avatars.githubusercontent.com/u/493257?v=4"]'
        ).should("not.exist");
        cy.get("debo").should("not.exist");
      });
    });
  });
});
