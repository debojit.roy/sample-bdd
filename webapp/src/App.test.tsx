import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders page heading correctly', () => {
  render(<App />);
  const linkElement = screen.getByText(/GitHub User Search/i);
  expect(linkElement).toBeInTheDocument();
});
