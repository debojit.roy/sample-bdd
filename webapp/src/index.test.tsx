import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';

let container: any;

beforeEach(() => {
  container = document.createElement('div');
  container.setAttribute('id', 'root');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = null;
});

it('mounts App on div', () => {
  const renderFunction =jest.spyOn(ReactDOM, 'render');
  act(() => { require('./index');  });
  expect(renderFunction).toHaveBeenCalled();
});