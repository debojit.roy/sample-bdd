import axios from 'axios';
import { searchUser } from '../searchUser';

jest.mock('axios');

describe('search User', () => {
  it('should search user and return details', async () => {
    const data = {
      total_count: 100,
      incomplete_results: false,
      items: [
        {
          login: 'debo',
          id: 1,
        },
        {
          login: 'debo2',
          id: 2,
        }
      ]
    };

    (axios as any).get.mockImplementationOnce(() => Promise.resolve({
      data,
      status: 200,
    }));

    const response = await searchUser('debo');
    expect(response).toEqual(data);
  });

  it('should throw error when API fails', async () => {
    const data = {
      error: 'Bad Request',
    };

    (axios as any).get.mockImplementationOnce(() => Promise.reject({
      data,
      status: 400,
    }));

    try {
      await searchUser('debo');
    } catch (error) {
      // eslint-disable-next-line jest/no-conditional-expect
      expect(error.status).toEqual(400);
      // eslint-disable-next-line jest/no-conditional-expect
      expect(error.data).toEqual(data);
    }
  });
});