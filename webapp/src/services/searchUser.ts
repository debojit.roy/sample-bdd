import axios from "axios";

export interface Result {
  login: string;
  id: number;
  node_id: string;
  avatar_url: string;
  gravatar_id: string;
  url: string;
  html_url: string;
  followers_url: string;
  following_url: string;
  gists_url: string;
  starred_url: string;
  subscriptions_url: string;
  organizations_url: string;
  repos_url: string;
  events_url: string;
  received_events_url: string;
  type: string;
  site_admin: boolean;
  score: number;
}

export interface SearchResponse {
  total_count: number;
  incomplete_results: boolean;
  items: Result[];
}

export const searchUser = async (username: string): Promise<SearchResponse> => {
  const response = await axios.get(
    `https://api.github.com/search/users?q=${username}`,
    {
      headers: {
        Accept: "application/vnd.github.v3+json",
      },
    }
  );

  return response.data;
};

const exportFunctions = {
  searchUser,
};

export default exportFunctions;
