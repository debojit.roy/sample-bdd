import React from 'react';
import './App.css';
import Search from './features/Search/components';

function App() {
  return (
    <div className="App container">
      <header className="mb-5">
        <h1>GitHub User Search</h1>
      </header>
      <section>
        <Search idSelector="search" />
      </section>
    </div>
  );
}

export default App;
