import React, { useState } from "react";

export interface SearchBarProps {
  idSelector: string;
  placeholderText: string;
  submitText: (searchText: string) => void;
}

const SearchBar: React.FC<SearchBarProps> = ({
  idSelector,
  placeholderText,
  submitText,
}) => {
  const [inputText, setInputText] = useState('');

  const handleEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if(event.key === 'Enter'){
      event.preventDefault();
      submitText(inputText);
    }
  }

  return (
    <input
      id={idSelector}
      data-id={idSelector}
      type="text"
      value={inputText}
      placeholder={placeholderText}
      onChange={event => setInputText(event.target.value)}
      onKeyPress={keyEvent => handleEnter(keyEvent)}
    />
  );
};

export default SearchBar;
