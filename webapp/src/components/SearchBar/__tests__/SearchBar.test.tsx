import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import user from '@testing-library/user-event'
import SearchBar from "../index";

describe('Search Bar', () => {
  test("renders correctly", () => {
    render(
      <SearchBar
        idSelector="test-bar"
        placeholderText="type text"
        submitText={() => {}}
      />
    );
    const searchBar = screen.getByRole("textbox");
    expect(searchBar).toBeInTheDocument();
    expect(searchBar).toHaveAttribute("placeholder", "type text");
  });
  
  test("calls the callback function when Enter is pressed", () => {
    const submit = jest.fn();
    render(
      <SearchBar
        idSelector="test-bar"
        placeholderText="type text"
        submitText={submit}
      />
    );
    const searchBar = screen.getByRole("textbox");
    user.type(searchBar, 'debo');
    fireEvent.keyPress(searchBar, { key: "Enter", code: 13, charCode: 13 });
    expect(submit.mock.calls.length).toBe(1);
    expect(submit.mock.calls[0][0]).toBe('debo');
  });

  test("NOT calls the callback function when other key is pressed", () => {
    const submit = jest.fn();
    render(
      <SearchBar
        idSelector="test-bar"
        placeholderText="type text"
        submitText={submit}
      />
    );
    const searchBar = screen.getByRole("textbox");
    user.type(searchBar, 'debo');
    fireEvent.keyPress(searchBar, { key: "Space", code: 32, charCode: 32 });
    expect(submit.mock.calls.length).toBe(0);
  });
});


