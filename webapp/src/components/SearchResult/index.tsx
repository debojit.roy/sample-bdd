import React from "react";

export interface SearchResultProps {
  id: number;
  username: string;
  avatarUrl: string;
}

const SearchResult: React.FC<SearchResultProps> = ({
  id,
  username,
  avatarUrl,
}) => {
  return (
    <>
      <img
        id={`${id}-image`}
        data-id={`${id}-image`}
        className="mr-3"
        src={avatarUrl}
        alt={username}
      />
      <div className="media-body">
        <h5 data-id={`${id}-username`} className="mt-0 mb-1">
          {username}
        </h5>
      </div>
    </>
  );
};

export default SearchResult;
