import React from "react";
import { render, screen } from "@testing-library/react";
import SearchResult from "../index";

describe('Search Result', () => {
  test('should render correctly and display image and username', () => {
    render(<li><SearchResult 
      id={1}
      avatarUrl="https://example.com/image.png"
      username="test_user"
    /></li>)
    const avatar = screen.getByRole('img');
    expect(avatar).toHaveAttribute('src', 'https://example.com/image.png');
    expect(avatar).toHaveAttribute('alt', 'test_user');
    
    const username = screen.getByText('test_user');
    expect(username).toBeInTheDocument();
  })
});