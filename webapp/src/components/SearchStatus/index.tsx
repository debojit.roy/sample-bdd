import React from "react";

export interface SearchStatusProps {
  text: string;
  isError?: boolean;
}

const SearchStatus: React.FC<SearchStatusProps> = ({ text, isError }) => {
  return <p className={`mb-5 ${isError ? "text-danger" : ""}`}>{text}</p>;
};

export default SearchStatus;
