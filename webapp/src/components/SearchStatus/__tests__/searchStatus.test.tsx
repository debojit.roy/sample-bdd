import React from "react";
import { render, screen } from "@testing-library/react";
import SearchStatus from "../index";

describe("Search Status", () => {
  test("should render correctly", () => {
    render(<SearchStatus text="this is test text" />);
    const searchStatus = screen.getByText('this is test text');
    expect(searchStatus).toBeInTheDocument();
  });

  test("should have danger class for error", () => {
    render(<SearchStatus text="this is an error" isError />);
    const searchStatus = screen.getByText('this is an error');
    expect(searchStatus).toHaveClass('text-danger')
  });
});
