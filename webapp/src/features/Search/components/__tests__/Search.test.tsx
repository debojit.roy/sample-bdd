import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import user from "@testing-library/user-event";
import { searchUser } from "../../../../services/searchUser";
import Search from "../index";

jest.mock("../../../../services/searchUser");

describe("Search", () => {
  test("renders textbox correctly", () => {
    render(<Search idSelector="test-bar" />);
    const searchBar = screen.getByRole("textbox");
    expect(searchBar).toBeInTheDocument();
    expect(searchBar).toHaveAttribute("placeholder", "Type Username");
  });

  test("type text and search user", async () => {
    render(<Search idSelector="test-bar" />);
    const searchBar = screen.getByRole("textbox");
    user.type(searchBar, 'debo');
    fireEvent.keyPress(searchBar, { key: "Enter", code: 13, charCode: 13 });

    const mockedResponse = Promise.resolve({
      total_count: 0,
      incomplete_results: false,
      items: [],
    });
    (searchUser as any).mockImplementationOnce(() => mockedResponse);
    await act(async () => { await mockedResponse });

    expect(searchUser).toHaveBeenCalledTimes(1);
    expect(searchUser).toHaveBeenCalledWith('debo');
  });

  test("should not search if text is empty", async () => {
    render(<Search idSelector="test-bar" />);
    const searchBar = screen.getByRole("textbox");
    user.type(searchBar, '');
    fireEvent.keyPress(searchBar, { key: "Enter", code: 13, charCode: 13 });

    const mockedResponse = Promise.resolve({
      total_count: 0,
      incomplete_results: false,
      items: [],
    });
    (searchUser as any).mockImplementationOnce(() => mockedResponse);
    expect(searchUser).not.toHaveBeenCalled();
  });

  test("should show results count after search", async () => {
    const mockedResponse = Promise.resolve({
      total_count: 100,
      incomplete_results: false,
      items: [],
    });

    (searchUser as any).mockImplementationOnce(
      () => mockedResponse
    );

    render(<Search idSelector="test-bar" />);
    const searchBar = screen.getByRole("textbox");
    user.type(searchBar, "debo");
    fireEvent.keyPress(searchBar, { key: "Enter", code: 13, charCode: 13 });

    await act(async () => {
      await mockedResponse;
    });
    expect(searchUser).toHaveBeenCalledTimes(1);
    expect(searchUser).toHaveBeenCalledWith("debo");
    const results = screen.getByText("100 Results Found");
    expect(results).toBeInTheDocument();
  });

  test("should show 0 Results if no results are found", async () => {
    const mockedResponse = Promise.resolve({
      total_count: 0,
      incomplete_results: false,
      items: [],
    });

    (searchUser as any).mockImplementationOnce(
      () => mockedResponse
    );

    render(<Search idSelector="test-bar" />);
    const searchBar = screen.getByRole("textbox");
    user.type(searchBar, "debo");
    fireEvent.keyPress(searchBar, { key: "Enter", code: 13, charCode: 13 });

    await act(async () => {
      await mockedResponse;
    });
    expect(searchUser).toHaveBeenCalledTimes(1);
    expect(searchUser).toHaveBeenCalledWith("debo");
    const results = screen.getByText("0 Results Found");
    expect(results).toBeInTheDocument();
  });

  test("should display error message if search fails", async () => {
    const mockedResponse = Promise.reject({
      status: 500,
      error: 'Failed to search'
    });

    (searchUser as any).mockImplementationOnce(
      () => mockedResponse
    );

    render(<Search idSelector="test-bar" />);
    const searchBar = screen.getByRole("textbox");
    user.type(searchBar, "debo");
    fireEvent.keyPress(searchBar, { key: "Enter", code: 13, charCode: 13 });

    await act(async () => {
      try {
        await mockedResponse;
      } catch (error) {
        // Do nothing
      }
    });
    const results = screen.getByText("Failed to search User");
    expect(results).toBeInTheDocument();
  });

  test('should display user avatar and name in search results', async () => {
    const mockedResponse = Promise.resolve({
      total_count: 2,
      incomplete_results: false,
      items: [{ 
        id: 1,
        login: 'user_one',
        avatar_url: 'https://example.com/image_1.png'
      },
      { 
        id: 2,
        login: 'user_two',
        avatar_url: 'https://example.com/image_2.png'
      }],
    });

    (searchUser as any).mockImplementationOnce(
      () => mockedResponse
    );

    render(<Search idSelector="test-bar" />);
    const searchBar = screen.getByRole("textbox");
    user.type(searchBar, "debo");
    fireEvent.keyPress(searchBar, { key: "Enter", code: 13, charCode: 13 });

    await act(async () => {
      await mockedResponse;
    });

    const avatars = screen.getAllByRole('img');
    expect(avatars[0]).toHaveAttribute('src', 'https://example.com/image_1.png');
    expect(avatars[0]).toHaveAttribute('alt', 'user_one');
    expect(avatars[1]).toHaveAttribute('src', 'https://example.com/image_2.png');
    expect(avatars[1]).toHaveAttribute('alt', 'user_two');

    const user_1 = screen.getByText('user_one');
    expect(user_1).toBeInTheDocument();
    const user_2 = screen.getByText('user_two');
    expect(user_2).toBeInTheDocument();
  })
});
