import React, { useEffect, useState } from "react";
import { searchUser, SearchResponse } from "../../../services/searchUser";
import SearchBar from "../../../components/SearchBar";
import SearchStatus from "../../../components/SearchStatus";
import SearchResult from "../../../components/SearchResult";
import './index.css';

export interface SearchProps {
  idSelector: string;
}

const Search: React.FC<SearchProps> = ({ idSelector }) => {
  const [searchText, setSearchText] = useState("");
  const [searchStatus, setSearchStatus] = useState<
    "NONE" | "PROGRESS" | "SUCCESS" | "FAILED"
  >("NONE");
  const [results, setSearchResults] = useState<SearchResponse>();

  const searchForUser = async (text: string) => {
    try {
      setSearchStatus("PROGRESS");
      const results = await searchUser(text);
      setSearchResults(results);
      setSearchStatus("SUCCESS");
    } catch (error) {
      setSearchStatus("FAILED");
    }
  };

  useEffect(() => {
    if (searchText && searchText.trim() !== "") {
      searchForUser(searchText);
    }
  }, [searchText]);

  return (
    <div className="container">
      <div className="row mb-5">
        <SearchBar
          idSelector={`${idSelector}-input`}
          placeholderText="Type Username"
          submitText={setSearchText}
        />
      </div>
      <div className="row mb-3">
        {searchStatus === "PROGRESS" && <SearchStatus text="In Progress" />}
        {searchStatus === "SUCCESS" && (
          <SearchStatus
            text={`${results ? results.total_count : 0} Results Found`}
          />
        )}
        {searchStatus === "FAILED" && (
          <SearchStatus text="Failed to search User" isError />
        )}
      </div>
      <div className="row">
        {results && results.items && results.items.length > 0 ? (
          <ul className="list-unstyled">
            {results.items.map((item) => (
              <li className="media mb-3" key={item.id}>
                <SearchResult
                  id={item.id}
                  username={item.login}
                  avatarUrl={item.avatar_url}
                />
              </li>
            ))}
          </ul>
        ) : null}
      </div>
    </div>
  );
};

export default Search;
