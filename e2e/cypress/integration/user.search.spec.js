describe('Github User Search', () => {
  it('A user lands on the home page', () => {
    cy.visit('http://localhost:3000')
  })

  it('the Browser Page Title should be - GitHub User Search', () => {
    cy.title().should('eq', 'GitHub User Search')
  })

  it('page Top Heading should be - GitHub User Search', () => {
    cy.get('h1').contains('GitHub User Search')
  })

  it('when the user has not focused on the search input, must see the default placeholder - Type Username', () => {
    cy.get('input').should('have.attr', 'placeholder', 'Type Username')
  })

  it('when the user has focused on the search input, the user must be able to type a username for search', () => {
    cy.get('input').type('tom');
  })

  context('when the user enters search text and hits enter', () => {
    it("should not search if the search text entered by the user is empty", () => {
      cy.get("input").clear().type("{enter}");
      cy.get("Results Found").should("not.exist");
    });

    it('should show the results count and user details', () => {
      let responseBody;

      cy.intercept("GET", "**/users?**", req => {
        req.reply((res) => {
          responseBody = res.body
        })
      }).as('getUser')

      cy.get("input").clear().type("debo").type("{enter}");
      cy.wait('@getUser').then(() => {
        console.log('Response: ', responseBody);
        cy.contains(`${responseBody.total_count} Results Found`);

        responseBody.items.forEach(item => {
          cy.get(
            `img[src="${item.avatar_url}"]`
          ).should("have.attr", "alt", item.login);
          cy.contains(item.login);
        });
      })
    });
  })
}) 